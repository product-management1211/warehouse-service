import { MigrationInterface, QueryRunner, TableColumn } from "typeorm"

export class addColumnToWarehouseTypeSettingTable1670317434580 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'warehouse_type_settings',
            new TableColumn({
                name: 'code',
                type: 'varchar',
                length: '4',
                isUnique: true
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE warehouse_type_settings DROP COLUMN code');
    }

}
