import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm"

export class createWarehouseInventoryItemTable1670230205751 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'warehouse_inventory_items',
                columns: [
                    {
                        name: 'inventory_id',
                        type: 'int',
                        isPrimary: true,
                    },
                    {
                        name: 'warehouse_id',
                        type: 'int',
                        isPrimary: true,
                    },
                    {
                        name: 'item_id',
                        type: 'int',
                        isPrimary: true,
                    },
                    {
                        name: 'plan_quantity',
                        type: 'decimal',
                        length: '10,2',
                    },
                    {
                        name: 'actutal_quantity',
                        type: 'decimal',
                        length: '10,2',
                    },
                    {
                        name: 'updated_at',
                        type: 'timestamptz',
                        default: 'now()',
                    },
                    {
                        name: 'created_at',
                        type: 'timestamptz',
                        default: 'now()',
                    },
                ],
            }),
            true,
        );

        await queryRunner.createForeignKey(
            'warehouse_inventory_items',
            new TableForeignKey({
                columnNames: ['inventory_id'],
                referencedTableName: 'inventories',
                referencedColumnNames: ['id'],
            }),
        );
        await queryRunner.createForeignKey(
            'warehouse_inventory_items',
            new TableForeignKey({
                columnNames: ['warehouse_id'],
                referencedTableName: 'warehouses',
                referencedColumnNames: ['id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable('warehouse_inventory_items');

        const foreignKey = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('warehouse_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_inventory_items', foreignKey);

        const foreignKey2 = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('inventory_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_inventory_items', foreignKey2);

        await queryRunner.dropTable('warehouse_inventory_items');
    }
}
