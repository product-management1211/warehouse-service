import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class createInventoryTable1670230167332 implements MigrationInterface {
    name = 'createInventoryTable1670230167332';
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'inventories',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'created_by_user_id',
                        type: 'int',
                    },
                    {
                        name: 'factory_id',
                        type: 'int',
                    },
                    {
                        name: 'approver_id',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'status',
                        type: 'int',
                        default: 1,
                    },
                    {
                        name: 'execution_day',
                        type: 'date',
                    },
                    {
                        name: 'updated_at',
                        type: 'timestamptz',
                        default: 'now()',
                    },
                    {
                        name: 'created_at',
                        type: 'timestamptz',
                        default: 'now()',
                    },
                ],
            }),
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('inventories');
    }

}
