import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm"

export class createWarehouseInventoryTable1670230194326 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'warehouse_inventories',
                columns: [
                    {
                        name: 'warehouse_id',
                        type: 'int',
                        isPrimary: true,
                    },
                    {
                        name: 'inventory_id',
                        type: 'int',
                        isPrimary: true,
                    },
                ],
            }),
            true,
        );
        await queryRunner.createForeignKey(
            'warehouse_inventories',
            new TableForeignKey({
                columnNames: ['warehouse_id'],
                referencedTableName: 'warehouses',
                referencedColumnNames: ['id'],
            }),
        );
        await queryRunner.createForeignKey(
            'warehouse_inventories',
            new TableForeignKey({
                columnNames: ['inventory_id'],
                referencedTableName: 'inventories',
                referencedColumnNames: ['id'],
                onDelete: 'CASCADE',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable('warehouse_inventories');
        const foreignKey = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('inventory_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_inventories', foreignKey);
        const foreignKey2 = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('warehouse_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_inventories', foreignKey2);
        await queryRunner.dropTable('warehouse_inventories');
    }

}
