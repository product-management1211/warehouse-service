import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm"

export class createWarehouseTypeTable1670320722394 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'warehouse_types',
                columns: [
                    {
                        name: 'warehouse_id',
                        type: 'int',
                        isPrimary: true,
                    },
                    {
                        name: 'warehouse_type_id',
                        type: 'int',
                        isPrimary: true,
                    },
                ],
            }),
            true,
        );
        await queryRunner.createForeignKey(
            'warehouse_types',
            new TableForeignKey({
                columnNames: ['warehouse_id'],
                referencedTableName: 'warehouses',
                referencedColumnNames: ['id'],
            }),
        );
        await queryRunner.createForeignKey(
            'warehouse_types',
            new TableForeignKey({
                columnNames: ['warehouse_type_id'],
                referencedTableName: 'warehouse_type_settings',
                referencedColumnNames: ['id'],
                onDelete: 'CASCADE',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable('warehouse_types');
        const foreignKey = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('warehouse_type_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_types', foreignKey);
        const foreignKey2 = table.foreignKeys.find(
            (fk) => fk.columnNames.indexOf('warehouse_id') !== -1,
        );
        await queryRunner.dropForeignKey('warehouse_types', foreignKey2);
        await queryRunner.dropTable('warehouse_types');
    }

}
