export default function isEmptyObject(obj) {
  for (const property in obj) {
    return false;
  }
  return true;
}
