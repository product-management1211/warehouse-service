import { getMessage, ResponseCodeEnum } from 'src/constants/response-code.enum';
import { ResponseBuilder } from './response-builder';
import { ResponsePayload } from './response-payload';

export class ApiError extends Error {
  private readonly _errorCode: ResponseCodeEnum;

  private readonly _message: string;

  private readonly _errorData: any;

  constructor(errorCode: ResponseCodeEnum, message?: string, errorData?: any) {
    super(message);

    this._errorCode = errorCode;
    this._message = message;
    this._errorData = errorData;
  }

  get errorCode(): ResponseCodeEnum {
    return this._errorCode;
  }

  get message(): string {
    return this._message || getMessage(this._errorCode);
  }

  get errorData(): any {
    return this._errorData;
  }

  toResponse(): ResponsePayload<unknown> {
    return new ResponseBuilder<unknown>()
      .withCode(this._errorCode)
      .withMessage(this.message)
      .build();
  }

  toResponseWithData(): ResponsePayload<unknown> {
    return new ResponseBuilder<unknown>()
      .withCode(this._errorCode)
      .withMessage(this.message)
      .withData(this.errorData)
      .build();
  }
}
