import { WarehouseTypeSettingRepositoryInterface } from "@components/warehouse-type-setting/interface/warehouse-type-setting.repository.interface";
import { BaseAbstractRepository } from "@core/repository/base.abstract.repository";
import { WarehouseTypeSetting } from "@entities/warehouse_type_setting/warehouse-type-setting.entity";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";


@Injectable()
export class WarehouseTypeSettingRepository
  extends BaseAbstractRepository<WarehouseTypeSetting>
  implements WarehouseTypeSettingRepositoryInterface
{
  constructor(
    @InjectRepository(WarehouseTypeSetting)
    private readonly warehouseTypeSettingRepository: Repository<WarehouseTypeSetting>,
  ) {
    super(warehouseTypeSettingRepository);
  }

  
}
