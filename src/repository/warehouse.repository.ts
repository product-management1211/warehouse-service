import { WarehouseRepositoryInterface } from '@components/warehouse/interface/warehouse.repository.interface';
import { BaseAbstractRepository } from '@core/repository/base.abstract.repository';
import { Warehouse } from '@entities/warehouse/warehouse.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WarehouseRepository
  extends BaseAbstractRepository<Warehouse>
  implements WarehouseRepositoryInterface
{
  constructor(
    @InjectRepository(Warehouse)
    private readonly warehouseRepository: Repository<Warehouse>,
  ) {
    super(warehouseRepository);
  }
  async getWarehousesByQuery(query: string) {
    console.log('query', query);
    return await this.warehouseRepository.manager.query(query);
  }
  async getWarehousesByName(request: any) {
    const warehouses = await this.warehouseRepository.manager.query(
      `SELECT * FROM warehouses WHERE LOWER(name) LIKE LOWER(unaccent('%hữu nghĩ%')) ESCAPE '\'`,
    );
    return warehouses;
  }
}
