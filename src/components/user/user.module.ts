import { ConfigService } from "@config/config.service";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { ClientProxyFactory } from "@nestjs/microservices";
import { UserService } from "./user.service";

@Module({
    imports: [ConfigModule],
    providers: [
        ConfigService,
        {
            provide: 'USER_SERVICE_CLIENT',
            useFactory: (configService: ConfigService) => {
                const userServiceOptions = configService.get('userService');
                return ClientProxyFactory.create(userServiceOptions);
            },
            inject: [ConfigService]
        },
        {
            provide: 'UserServiceInterface',
            useClass: UserService,
        },
    ],
    exports: [
        'USER_SERVICE_CLIENT',
        {
            provide: 'UserServiceInterface',
            useClass: UserService,
        }
    ]
})
export class UserModule { }