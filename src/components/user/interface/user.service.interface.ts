export interface UserServiceInterface {
  getCompanyById(id: number): Promise<any>;
  // getUserByIds(condition, serilize?: boolean): Promise<any>;
  // getFactoriesByIds(condition, serilize?: boolean): Promise<any>;
  getFactoriesByConditions(conditions): Promise<any>;
  // deleteRecordUserWarehouses(condition): Promise<any>;
  // getUsersByConditions(condition): Promise<any>;
  // getUsersByUsernameOrFullName(filterByUser, onlyId?): Promise<any>;
}
