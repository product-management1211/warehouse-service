import { Inject, Injectable } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { lastValueFrom } from "rxjs";
import { UserServiceInterface } from "./interface/user.service.interface";

@Injectable()
export class UserService implements UserServiceInterface {
    constructor(
        @Inject('USER_SERVICE_CLIENT')
        private readonly userServiceClient: ClientProxy,
    ) { }
    async getFactoriesByConditions(conditions: any): Promise<any> {
        console.log('hello 1');
        
        const response = await lastValueFrom(this.userServiceClient.
            send('get_factories_by_conditions', { conditions })
        )

        console.log('hello 2');
        return response;
    }

    async getCompanyById(id: number): Promise<any> {
        console.log('hello 1');
        const response = await lastValueFrom(this.userServiceClient.send('get_company_by_id', { id }));
        console.log('hello 2');
        return response;
    }
}