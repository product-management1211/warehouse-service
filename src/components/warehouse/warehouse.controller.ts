import { Body, Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ResponsePayload } from '@utils/response-payload';
import { isEmpty } from 'lodash';
import { CreateWarehouseRequestDto } from './dto/request/create-warehouse.request.dto';
import { GetWarehousesRequestDto } from './dto/request/get-warehouses-request.dto';
import { WarehouseResponseDto } from './dto/response/warehouse.response.dto';
import { WarehouseServiceInterface } from './interface/warehouse.service.interface';

@Controller('warehouses')
export class WarehouseController {
  constructor(
    @Inject('WarehouseServiceInterface')
    private readonly warehouseService: WarehouseServiceInterface,
  ) {}

  @MessagePattern('create')
  public async create(
    @Body() payload: CreateWarehouseRequestDto,
  ): Promise<ResponsePayload<WarehouseResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.warehouseService.create(request);
  }

  @MessagePattern('get_warehouses_by_ids')
  public async getWarehouseByIds(
    @Body() payload: GetWarehousesRequestDto,
  ): Promise<any> {
    return await this.warehouseService.getListByIds(payload);
  }

  @MessagePattern('get_warehouses_using_query')
  public async getWarehousesByNames(request: any): Promise<any> {
    return await this.warehouseService.getWarehousesUsingQuery(request.query);
  }

  @MessagePattern('get_warehouses_by_conditions')
  public async getWarehousesByCondition(condition: any): Promise<any> {
    return await this.warehouseService.getListByConditions(condition);
  }
}
