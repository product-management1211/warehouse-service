import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BaseDto } from '@core/dto/base.dto';
import {
  ArrayUnique,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class WarehouseTypeSettings {
  @ApiProperty({ example: 1, description: 'warehouse type id' })
  @IsInt()
  @IsNotEmpty()
  id: number;
}

export class CreateWarehouseRequestDto extends BaseDto {
  @ApiProperty({ example: 'Warehouse 1', description: 'warehouse name' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @ApiProperty({ example: 1, description: 'warehouse factoryId' })
  @IsInt()
  @IsNotEmpty()
  factoryId: number;

  @ApiProperty({ example: 1, description: 'warehouse companyId' })
  @IsInt()
  @IsNotEmpty()
  companyId: number;

  @ApiProperty({ example: 'ABCD', description: 'warehouse code' })
  @IsString()
  @MaxLength(4)
  @IsNotEmpty()
  code: string;

  @ApiProperty({
    example: 'This is warehouse 1',
    description: 'warehouse description',
  })
  @IsString()
  @MaxLength(255)
  @IsOptional()
  description: string;

  @ApiProperty({ example: 'Ha Noi City', description: 'warehouse location' })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  location: string;

  @ApiProperty({ type: [WarehouseTypeSettings] })
  @IsOptional()
  @IsArray()
  @ArrayUnique<WarehouseTypeSettings>((e: WarehouseTypeSettings) => e.id)
  @Type(() => WarehouseTypeSettings)
  warehouseTypeSettings: WarehouseTypeSettings[];
}
