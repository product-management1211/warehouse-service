import { BaseDto } from '@core/dto/base.dto';
import { ArrayNotEmpty, IsOptional } from 'class-validator';

export class GetWarehousesRequestDto extends BaseDto {
  @ArrayNotEmpty()
  warehouseIds: number[];

  @IsOptional()
  relation: string[];
}
