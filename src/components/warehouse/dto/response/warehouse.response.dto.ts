import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';


export class Factory {
  @Expose()
  id: number;

  @Expose()
  name: string;

  @Expose()
  code: string;
}

export class WarehouseTypeSetting {
  @Expose()
  id: number;

  @Expose()
  name: string;
}

export class WarehouseResponseDto {
  @ApiProperty({ example: 1, description: 'warehouse id' })
  @Expose()
  id: number;

  @ApiProperty({ example: 'warehouse 1', description: 'warehouse name' })
  @Expose()
  name: string;

  @ApiProperty({ example: 1, description: 'warehouse factoryId' })
  @Expose()
  factoryId: number;

  @ApiProperty({ example: 1, description: 'warehouse companyId' })
  @Expose()
  companyId: number;

  @ApiProperty({ example: 'ABCD', description: 'warehouse code' })
  @Expose()
  code: string;

  @ApiProperty({
    example: 'This is warehouse 1',
    description: 'warehouse description',
  })
  @Expose()
  description: string;

  @ApiProperty({ example: 'Ha Noi City', description: 'warehouse location' })
  @Expose()
  location: string;

  @ApiProperty({ example: 1, description: 'warehouse status' })
  @Expose()
  status: number;

  @ApiProperty({ example: 1, description: 'warehouse factory' })
  @Expose()
  @Type(() => Factory)
  factory: Factory;

  @ApiProperty({
    example: [{ id: 1 }],
    description: 'warehouse type',
  })
  @Expose()
  @Type(() => WarehouseTypeSetting)
  warehouseTypeSettings: WarehouseTypeSetting[];
}

