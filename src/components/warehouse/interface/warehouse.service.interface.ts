import { Warehouse } from '@entities/warehouse/warehouse.entity';
import { ResponsePayload } from '@utils/response-payload';
import { CreateWarehouseRequestDto } from '../dto/request/create-warehouse.request.dto';
import { GetWarehousesRequestDto } from '../dto/request/get-warehouses-request.dto';
import { WarehouseResponseDto } from '../dto/response/warehouse.response.dto';

export interface WarehouseServiceInterface {
  create(
    payload: CreateWarehouseRequestDto,
  ): Promise<ResponsePayload<WarehouseResponseDto | any>>;

  getListByIds(
    warehouseIds: GetWarehousesRequestDto,
  ): Promise<Warehouse[] | ResponsePayload<any>>;

  getWarehousesUsingQuery(query: string): Promise<any>;
  getListByConditions(conditions: any): Promise<any>;
}
