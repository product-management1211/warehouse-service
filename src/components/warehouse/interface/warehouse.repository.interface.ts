import { BaseInterfaceRepository } from '@core/repository/base.interface.repository';
import { Warehouse } from '@entities/warehouse/warehouse.entity';

export interface WarehouseRepositoryInterface
  extends BaseInterfaceRepository<Warehouse> {
  getWarehousesByQuery(query: string);
  // getList(request: GetListWarehouseRequestDto);
  // getWarehouseInventoryList(request: GetWarehouseInventoryListRequestDto);
  // getDetail(id: number);
  // delete(id: number);
}
