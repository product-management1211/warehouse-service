import { UserService } from '@components/user/user.service';
import { WarehouseTypeSettingRepositoryInterface } from '@components/warehouse-type-setting/interface/warehouse-type-setting.repository.interface';
import { Warehouse } from '@entities/warehouse/warehouse.entity';
import { Inject, Injectable } from '@nestjs/common';
import { ApiError } from '@utils/api.error';
import { ResponseBuilder } from '@utils/response-builder';
import { ResponsePayload } from '@utils/response-payload';
import { plainToInstance } from 'class-transformer';
import { isEmpty, unionBy } from 'lodash';
import { I18nService } from 'nestjs-i18n';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { In } from 'typeorm';
import { CreateWarehouseRequestDto } from './dto/request/create-warehouse.request.dto';
import { GetWarehousesRequestDto } from './dto/request/get-warehouses-request.dto';
import { WarehouseResponseDto } from './dto/response/warehouse.response.dto';
import { WarehouseRepositoryInterface } from './interface/warehouse.repository.interface';
import { WarehouseServiceInterface } from './interface/warehouse.service.interface';

@Injectable()
export class WarehouseService implements WarehouseServiceInterface {
  constructor(
    @Inject('WarehouseRepositoryInterface')
    private readonly warehouseRepository: WarehouseRepositoryInterface,

    @Inject('WarehouseTypeSettingRepositoryInterface')
    private readonly warehouseTypeSettingRepository: WarehouseTypeSettingRepositoryInterface,

    @Inject('UserServiceInterface')
    private readonly userService: UserService,

    private readonly i18n: I18nService,
  ) {}
  async getListByConditions(conditions: any): Promise<any> {
    return await this.warehouseRepository.findByCondition({
      where: conditions,
    });
  }

  async getWarehousesUsingQuery(query: string): Promise<any> {
    const warehouses = await this.warehouseRepository.getWarehousesByQuery(
      query,
    );
    return warehouses;
  }

  public async create(
    payload: CreateWarehouseRequestDto,
  ): Promise<ResponsePayload<WarehouseResponseDto | any>> {
    try {
      await this.checkWarehouseRequest(payload);
      const createdWarehouse = await this.warehouseRepository.create(payload);

      const response = plainToInstance(WarehouseResponseDto, createdWarehouse, {
        excludeExtraneousValues: true,
      });

      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.SUCCESS)
        .withMessage(await this.i18n.translate('error.SUCCESS'))
        .withData(response)
        .build();
    } catch (error) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate(error.message || error))
        .build();
    }
  }

  private async checkWarehouseRequest(payload: CreateWarehouseRequestDto) {
    const companyResponse = await this.userService.getCompanyById(
      payload.companyId,
    );
    if (
      companyResponse.statusCode === ResponseCodeEnum.SUCCESS &&
      companyResponse.data === null
    ) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_NOT_FOUND'),
      );
    }

    const factoryResponse = await this.userService.getFactoriesByConditions({
      id: payload.factoryId,
      companyId: payload.companyId,
    });
    if (
      factoryResponse.statusCode === ResponseCodeEnum.SUCCESS &&
      factoryResponse.data.length === 0
    ) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.FACTORY_NOT_FOUND'),
      );
    }

    const isUniqueCode = await this.checkUniqueCode(payload.code);
    if (!isUniqueCode) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.CODE_IS_ALREADY_EXISTS'),
      );
    }

    const warehouseTypeIds = unionBy(
      payload.warehouseTypeSettings.map((wareHouse) => wareHouse.id),
    );
    const warehouseTypeList =
      await this.warehouseTypeSettingRepository.findByCondition({
        id: In(warehouseTypeIds),
      });
    if (
      isEmpty(warehouseTypeList) ||
      warehouseTypeList.length !== warehouseTypeIds.length
    ) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.WAREHOUSE_TYPE_NOT_FOUND'),
      );
    }
  }

  /**
   * Check warehouse code unique
   * @param code
   * @param id
   * @returns
   */
  private async checkUniqueCode(code: string, id?: number): Promise<boolean> {
    const result = await this.warehouseRepository.findByCondition([
      { code: code },
    ]);
    return result.length === 0;
  }

  async getListByIds(
    payload: GetWarehousesRequestDto,
  ): Promise<Warehouse[] | ResponsePayload<any>> {
    console.log('warehouses', payload.warehouseIds);

    const warehouses = await this.warehouseRepository.findWithRelations({
      where: {
        id: In(payload.warehouseIds),
      },
      relations: payload.relation,
    });

    if (!warehouses.length) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.NOT_FOUND)
        .withMessage(await this.i18n.translate('error.NOT_FOUND'))
        .build();
    }

    return new ResponseBuilder(warehouses)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withMessage(await this.i18n.translate('error.SUCCESS'))
      .build();
  }
}
