import { UserModule } from "@components/user/user.module";
import { UserService } from "@components/user/user.service";
import { Warehouse } from "@entities/warehouse/warehouse.entity";
import { WarehouseTypeSetting } from "@entities/warehouse_type_setting/warehouse-type-setting.entity";
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { WarehouseTypeSettingRepository } from "src/repository/warehouse-type-setting.repository";
import { WarehouseRepository } from "src/repository/warehouse.repository";
import { WarehouseController } from "./warehouse.controller";
import { WarehouseService } from "./warehouse.service";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Warehouse,
            WarehouseTypeSetting
        ]),
        UserModule
    ],
    providers: [
        {
            provide: 'WarehouseRepositoryInterface',
            useClass: WarehouseRepository,
        },
        {
            provide: 'WarehouseServiceInterface',
            useClass: WarehouseService
        },
        {
            provide: 'WarehouseTypeSettingRepositoryInterface',
            useClass: WarehouseTypeSettingRepository,
          },
        {
            provide: 'UserServiceInterface',
            useClass: UserService,
        },
    ],
    controllers: [WarehouseController]
})
export class WarehouseModule { }