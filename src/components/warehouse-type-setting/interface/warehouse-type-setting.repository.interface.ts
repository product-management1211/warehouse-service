import { BaseInterfaceRepository } from '@core/repository/base.interface.repository';
import { WarehouseTypeSetting } from '@entities/warehouse_type_setting/warehouse-type-setting.entity';

export interface WarehouseTypeSettingRepositoryInterface
  extends BaseInterfaceRepository<WarehouseTypeSetting> {
  // getList(request: GetListWarehouseTypeSettingRequestDto);
  // getDetail(id: number);
  // createEntity(
  //   warehouseTypeSettingDto: WarehouseTypeSettingRequestDto,
  // ): Promise<WarehouseTypeSetting>;
}
