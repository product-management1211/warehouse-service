import { Warehouse } from '@entities/warehouse/warehouse.entity';
import { WarehouseItemTypeSetting } from '@entities/warehouse-item-type-setting/warehouse-item-type-setting.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'warehouse_type_settings' })
export class WarehouseTypeSetting {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 4,
  })
  code: string;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  description: string;

  @UpdateDateColumn({
    type: 'timestamptz',
  })
  updatedAt: Date;

  @CreateDateColumn({
    type: 'timestamptz',
  })
  createdAt: Date;

  @ManyToMany(() => Warehouse)
  @JoinTable({
    name: 'warehouse_types',
    joinColumn: {
      name: 'warehouse_type_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'warehouse_id',
      referencedColumnName: 'id',
    },
  })
  warehouses: Warehouse[];

  @OneToMany(
    () => WarehouseItemTypeSetting,
    (warehouseItemTypeSetting) => warehouseItemTypeSetting.WarehouseTypeSetting,
  )
  @JoinColumn({ name: 'id', referencedColumnName: 'warehouse_type_id' })
  WarehouseItemTypeSettings: WarehouseItemTypeSetting[];
}
