import { Inventory } from '@entities/inventory/inventory.entity';
import { WarehouseTypeSetting } from '@entities/warehouse_type_setting/warehouse-type-setting.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'warehouses' })
export class Warehouse {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  name: string;

  @Column({
    type: 'integer',
    nullable: false,
  })
  factoryId: number;

  @Column({
    type: 'integer',
    nullable: false,
  })
  companyId: number;

  @Column({
    type: 'varchar',
    length: 4,
  })
  code: string;

  @Column({
    type: 'varchar',
    length: 255,
  })
  description: string;

  @Column({
    type: 'varchar',
    length: 255,
  })
  location: string;

  @Column({
    type: 'integer',
    default: 1,
  })
  status: number;

  @UpdateDateColumn({
    type: 'timestamptz',
  })
  updatedAt: Date;

  @CreateDateColumn({
    type: 'timestamptz',
  })
  createdAt: Date;

  @ManyToMany(() => Inventory)
  @JoinTable({
    name: 'warehouse_inventories',
    joinColumn: {
      name: 'warehouse_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'inventory_id',
      referencedColumnName: 'id',
    },
  })
  inventories: Inventory[];

  @ManyToMany(() => WarehouseTypeSetting)
  @JoinTable({
    name: 'warehouse_types',
    joinColumn: {
      name: 'warehouse_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'warehouse_type_id',
      referencedColumnName: 'id',
    },
  })
  warehouseTypeSettings: WarehouseTypeSetting[];
}
