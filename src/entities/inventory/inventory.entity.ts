import { Warehouse } from '@entities/warehouse/warehouse.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity({ name: 'inventories' })
export class Inventory {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'varchar',
    unique: true,
    length: 12,
  })
  code: string;

  @Column({
    type: 'varchar',
    length: 255,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  description: string;

  @Column({
    type: 'integer',
  })
  createdByUserId: number;

  @Column({
    type: 'integer',
    nullable: true,
  })
  factoryId: number;

  @Column({
    type: 'integer',
    nullable: true,
  })
  approverId: number;

  @Column({
    type: 'integer',
    nullable: true,
  })
  confirmerId: number;

  @Column({
    type: 'integer',
    nullable: false,
    default: 1,
  })
  status: number;

  @Column({
    type: 'date',
  })
  executionDay: string;

  @Column({
    type: 'date',
    nullable: true,
  })
  confirmedAt: string;

  @Column({
    type: 'date',
    nullable: true,
  })
  approvedAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @CreateDateColumn()
  createdAt: string;

  @ManyToMany(() => Warehouse)
  @JoinTable({
    name: 'warehouse_inventories',
    joinColumn: {
      name: 'inventory_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'warehouse_id',
      referencedColumnName: 'id',
    },
  })
  warehouses: Warehouse[];
}
