import { Inventory } from '@entities/inventory/inventory.entity';
import { Warehouse } from '@entities/warehouse/warehouse.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'warehouse_inventory_items' })
export class WarehouseInventoryItem {
  @PrimaryColumn()
  inventoryId: number;

  @PrimaryColumn()
  warehouseId: number;

  @PrimaryColumn()
  itemId: number;

  @Column({
    type: 'decimal',
  })
  planQuantity: number;

  @Column({
    type: 'decimal',
  })
  actualQuantity: number;

  @UpdateDateColumn({
    type: 'timestamptz',
  })
  updatedAt: Date;

  @CreateDateColumn({
    type: 'timestamptz',
  })
  createdAt: Date;

  @ManyToOne(() => Inventory, (inventory) => inventory.id)
  @JoinColumn({ name: 'inventory_id', referencedColumnName: 'id' })
  inventory: Inventory;

  @ManyToOne(() => Warehouse, (warehouse) => warehouse.id)
  @JoinColumn({ name: 'warehouse_id', referencedColumnName: 'id' })
  warehouse: Warehouse;
}
