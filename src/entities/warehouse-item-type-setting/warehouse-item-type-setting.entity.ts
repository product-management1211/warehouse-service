import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { WarehouseTypeSetting } from '@entities/warehouse_type_setting/warehouse-type-setting.entity';

@Entity({ name: 'warehouse_item_type_settings' })
export class WarehouseItemTypeSetting {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'int',
  })
  warehouseTypeId: number;

  @Column({
    type: 'int',
  })
  itemTypeId: number;

  @UpdateDateColumn({
    type: 'timestamptz',
  })
  updatedAt: Date;

  @CreateDateColumn({
    type: 'timestamptz',
  })
  createdAt: Date;

  @ManyToOne(
    () => WarehouseTypeSetting,
    (warehouseTypeSetting) => warehouseTypeSetting.WarehouseItemTypeSettings,
  )
  @JoinColumn({ name: 'warehouse_type_id', referencedColumnName: 'id' })
  WarehouseTypeSetting: WarehouseTypeSetting;
}
