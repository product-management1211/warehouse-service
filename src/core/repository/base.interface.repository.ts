import { DeleteResult } from 'typeorm';

export interface BaseInterfaceRepository<T> {
  createEntity(data: any): T;

  create(data: T | any): Promise<T>;

  update(data: T | any): Promise<T>;

  delete(id: number): Promise<DeleteResult>;

  findOneByCondition(filterCondition: any): Promise<T>;

  findByCondition(filterCondition: any): Promise<T[]>;

  findWithRelations(relations: any): Promise<T[]>;

  findAll(): Promise<T[]>;
}
