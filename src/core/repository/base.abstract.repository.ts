import { DeleteResult, Repository } from 'typeorm';
import { BaseInterfaceRepository } from './base.interface.repository';

export abstract class BaseAbstractRepository<T>
  implements BaseInterfaceRepository<T>
{
  private entity: Repository<T>;

  protected constructor(entity: Repository<T>) {
    this.entity = entity;
  }

  createEntity(data: any): T {
    throw new Error('Method not implemented.');
  }

  public async create(data: T | any): Promise<T> {
    return await this.entity.save(data);
  }

  public async update(data: any): Promise<T> {
    return await this.entity.save(data);
  }

  public async delete(id: number): Promise<DeleteResult> {
    return await this.entity.delete(id);
  }

  public async findOneByCondition(filterCondition: any): Promise<T> {
    return await this.entity.findOne(filterCondition);
  }

  public async findByCondition(filterCondition: any): Promise<T[]> {
    return await this.entity.find(filterCondition);
  }

  public async findWithRelations(relations: any): Promise<T[]> {
    return await this.entity.find(relations);
  }

  public async findAndCount(filterCondition: any): Promise<any> {
    return await this.entity.findAndCount(filterCondition);
  }

  public async findAll(): Promise<T[]> {
    return await this.entity.find();
  }
}
