import { Transport } from "@nestjs/microservices";

export class ConfigService {
  private readonly envConfig: { [key: string]: any } = null;

  constructor() {
    this.envConfig = {
      port: process.env.SERVER_PORT,
    };

    this.envConfig.userService = {
      options: {
        port: process.env.USER_SERVICE_PORT || 3000, 
        host: process.env.USER_SERVICE_HOST || 'user-service'
      },
      transport: Transport.TCP
    }
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}
